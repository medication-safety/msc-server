## OWL Server

**Resourcen:** 

Putty 

WinSCP

Sudo Rigths 

**Execution:**

For the first time: copy all files(bak, demo, tmp, webapps, deploy.sh from your predecessor in your own file (/home/myname)

<p align="center">
  <img width="600" src="https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/owl-server.png">
</p>

Copy the new generated WAR in the file „tmp“

Execute the shellscript „deploy.sh“ (Execution command: „deploy.sh“)

Note: the tomcat server can be online all the time

Check the URL: http://safety-code.org/msc3/generate, if everything works fine.