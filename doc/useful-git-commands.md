## Useful git commands

Git clone: https://gitlab.com/medication-safety/msc-server.git:Copy msc-server to the local PC

Git checkout: Switch between the branch

Git diff: Displays changes in detail

Git status: shows which files have changed

Git add . : Accepts all changes

Git commit -m: Left a comment what has changed

Git push : all changes are transferred to Gitlab

Git pull: Gets the current version from Gitlab

Git stash: Change branch without commit

Git stash list: a list of storage in stash

Git stash apply: apply to a version in stash

Git stash drop: delete the content in stash

Git commit -a: Comments and the changes are saved in the stash
