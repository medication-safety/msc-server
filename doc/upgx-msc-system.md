# UPGX-MSC-SYSTEM

The latest version is in branch „feature-upgx-card” on Gitlab (gitlab.com/medication-safety/msc-server/-/tree/feature-upgx-card)

The main difference between master branch and “feature-upgx-card” are the two files:

+ [UPGxPocketCardServerResource.java](https://gitlab.com/medication-safety/msc-server/-/blob/feature-upgx-card/msc3/src/main/java/org/safetycode/msc3/resources/UPGxPocketCardServerResource.java)

+ UPGxTemplates_TextFragments_Textfieldposition.java 

 

The pdf-templates of all sites are saved in msc-server\msc3\src\main\webapp\WEB-INF\classes.

Also the deault version of the pocket card:

![img](https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/upgx-1.png)

Image 1: Default version

The frontside of the pocket card is hard coded. **Use PhantomPDF/FoxitPDF to process the pocket card.** Other pdf editors can change the parameters and resolution.

 

The required information for the backside of the pocket card is determined via JSON

Default JSON structure:

![img](https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/upgx-2.png)

An Example of the pocket card:

<p align="center">
  <img width="600" src="https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/upgx-3.png">
</p>

**UPGxPocketCardServerResoure.java**

The QR-code in front side of the pocket card is generated with the url passed to json.

Mostly this java class is used to generate the back of the pocket card. 

![img](https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/upgx-4.png)

Image 2: Shows back side of the card

The back side is divided in 4 sectors, one is patient information like name, date of birth and id. By default values from json request, these class generate textfields for each of the default values. Sector 2 displays genes and phenotypes, sector 3 displays all active ingredients and additional information (see below “order of displaying”), sector 4 timestamp.

·     Generates two tables for the backside of the card first one with font size 8 and the second with font size 7

·     After the compare of the both tables, the table with most genes will be displayed.

·     The maximum numbers of genes that fit on the backside are between 40 and 45.

·     A rollback system to display the maximum actionable genes with recommendation.

 

If there is a recommendation for CYP2D6, the gen will always displayed at first, other genes with recommendation follow alphabetically.

**The order of displaying:** 

·     Actionable Genes: all genes with recommendation will be displayed, CYP2D6 with the highest priority

 

·     Other Actionable Genes: if actionable genes do not fit on the backside, the latest actionable gen will be removed and instead a list of actionable genes without phenotype and recommendations

·     (Future version a list of partial genes will be displayed)

·     Non Actionable Genes: Displays other tested genes without recommendation, if it fits on the card

·     No guidelines apply: Is displayed if there are no recommendations.

 

**UPGxTemplates_TextFragments_Textfieldposition.java**

**Template IDs of the sites**

| **Template**                                                 | **ID** |
| ------------------------------------------------------------ | ------ |
| POCKET_CARD_TEMPLATE = "pocket_card_template.pdf";   | 0      |
| POCKET_CARD_TEMPLATE_GR1 = "pocket_card_template_gr.pdf"; | 1      |
| POCKET_CARD_TEMPLATE_GR2 = "pocket_card_template_gr2.pdf"; | 2      |
| POCKET_CARD_TEMPLATE_GR3 = "pocket_card_template_gr3.pdf"; | 10     |
| POCKET_CARD_TEMPLATE_GR4 = "pocket_card_template_gr4.pdf"; | 11     |
| POCKET_CARD_TEMPLATE_SL = "pocket_card_template_sl.pdf"; | 3      |
| POCKET_CARD_TEMPLATE_SP = "pocket_card_template_sp.pdf"; | 4      |
| POCKET_CARD_TEMPLATE_NL = "pocket_card_template_nl.pdf"; | 7      |
| POCKET_CARD_TEMPLATE_EN = "pocket_card_template_en.pdf"; | 9      |
| POCKET_CARD_TEMPLATE_IT1 = "pocket_card_template_it1.pdf"; | 5      |
| POCKET_CARD_TEMPLATE_IT2 = "pocket_card_template_it2.pdf"; | 6      |
| POCKET_CARD_TEMPLATE_AT = "pocket_card_template_at.pdf"; | 8      |

 

For each case of “the order of displaying” there are text fragments for all seven language. Greece letter has to be in ASCII code. Currently 4 templates for Greek, 2 for Italy differ only on the front side of the pocket card (logo and address). Other sites have one template, the default template shows a blank front side of the pocket card.

 

**Common.java**

·     Defines the path like URL, path of the templates, etc.

·     Size of QR-Code

·     Size of the tables on the back side of the pocket card

·     Colour of the borders, currently (76, 139, 179)

 

**JSONUPGX.txt**

Generate testing cards for all cases in all language

 