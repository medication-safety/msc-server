# Short Introduction of the MSC-system

A phytonscript "generate properties.py" was written, which created various properties under „resources /config /“. The Phytonscript reads the necessary datasets from a CSV-file and saves and arranges them in the corresponding categories genes, guidelines, phenotypes, haplotypes and medications. 

![](https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/short-intro-1.png)

Package populate defines the getMethod() for each of the created properties. 

Package representation defines the relationships between genes, guidelance, phenotypes, haplotypes, and drugs. The URL fragment generation is defined in Profile.java.

In SafetyCodeApplication you can find the paths of the routers.

Package util: Common.java defines

·     the prefix URL, 

·     the size of the QR code, 

·     table size on the back site of the card, 

·     the paths of the properties 

·     the paths of the PDF template

Package resources: the URL is created in GenerateServerResource. Design of the MSC card is performed in PocketCardServerResource.

 

URL Generation:

6 digits:

·     2 digits for phenotype

·     2 digits for haplotype-1

·     2 digits for haplotype-2

![](https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/short-intro-2.png)

**Gitlab**

Except master there are two other active branches: „feature-upgx-card“ and "feature json"

For each feature a extra class defines the MSC card

UPGxPocketCardServerResource in feature-upgx-card“ and JSON_PocketCardServerResource in "feature json"

The card template is adapted to the printer in "feature-ugpx-card".

For the two other branches the card template still have to be adjusted if somebody wants to print on a plastic card.

**Update Gradle and Groovy**

The versions of gradle and groovy can be updated in „build.gradle“.

# Local execution guide of MSC

#### Resources

The following resources are required for the local execution of MSC-system:

+ **Git:** https://git-scm.com/downloads

+ **Tomcat:** http://tomcat.apache.org/

+ **Java:** http://www.oracle.com/technetwork/java/javase/downloads/index.html 

Note: tomcat and java have to be compatible.

Don’t forget to define path for JAVA_HOME

**(Development enviroment:** I prefer Intellij IDEA, but you can use anyone.)

#### Local execution

Extract the MSC-Server.zip

Open Git(CMD(for windwows), MINGW(for Linux))

Change the path to the folder msc3

Execute the build command for windows: gradlew.bat build 

and for linux: ./gradlew build 

(For the first time Gradle will be installed automatically)

![](https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/loc-exec-1.png)

If the build was succesful, a WAR-file msc3 will be generated in the folder libs. 

<p align="center">
  <img width="600" src="https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/loc-exec-2.png">
</p>

Start tomcat: (https://tomcat.apache.org/tomcat-4.1-doc/RUNNING.txt)

Deploy the the war file msc3 in your tomcat (URL should be: localhost:8080/manager)

(For the first time: you have to change username and password. https://www.mkyong.com/tomcat/tomcat-default-administrator-password/)

<p align="center">
  <img width="600" src="https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/loc-exec-3.png">
</p>

#### To generate a MSC-Card (Website)

Go to: http://localhost:8080/msc3/generate

<p align="center">
  <img width="600" src="https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/loc-exec-4.png">
</p>

To activate a gene, click on the button with the gene name, the button should turn blue.  Choose a phenotype and (optional) haplotype. 

If you finish click on the „Submit“-Button.

<p align="center">
  <img width="600" src="https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/loc-exec-5.png">
</p>

Two URLs are displayed, **BUT** both of them point to the server of medical university of Vienna.

To generate a local MSC-card, please follow one of the two steps:

Step 1: Easiest way, to get your local MSC-card.

Delete the URL „http://safety-code.org“
Instead of the URL write: „localhost:8080“

![](https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/loc-exec-6.png)

Step 2:

Change the string URL_PREFIX „http://safety-code.org/msc3/“  in Common.java to „localhost:8080/msc3/“

Advantage: No manual inputs are required

#### To generate a MSC-Card (JSON)

+ Branch feature-json

  As an example two JSON-files are already prepared.
  Execute samples_of_pocket_cards_all_sites.txt in the folder msc3, it should generated two PDFs in msc3. 
  (The execution is only in MINGW possible  because of the curl command.)
  (If the port isn’t 8080 please fit the curl command)
  
  ![](https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/loc-exec-json-1.png)

#### Change settings of the MSC-card

Path of the pocket_card_template.pdf

<p align="center">
  <img width="600" src="https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/loc-exec-7.png">
</p>

Path of the PocketCardServerResource.java

<p align="center">
  <img width="600" src="https://gitlab.com/medication-safety/msc-server/-/raw/master/doc/img/loc-exec-8.png">
</p>