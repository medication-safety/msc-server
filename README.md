### Let's make data on essential pharmacogenes available for every patient everywhere!

The availability of pharmacogenomic data of individual patients can significantly improve physicians' prescribing behaviour and lead to a **reduced incidence of adverse drug events** and an **improvement of effectiveness of treatment**. Unfortunately, **major barriers** to the wide-spread implementation of pharmacogenomics in clinical practice remain:

* Significant financial cost and turnaround time as well as limited availability of indication-specific pharmacogenomic tests.
* Difficulties in the storage, exchange, and interpretation of pharmacogenetic data.  In particular, interoperable electronic health record systems that are capable of storing structured genetic test results and transmitting those results to other care providers are needed; unfortunately, this type of infrastructure is not available in most parts of the world.
* A lack of easily accessible computer-based clinical decision support systems that can assist clinicians with the interpretation of pharmacogenetic data – a vital component to the use of these data at the point of care.


### A possible solution: The Medication Safety Code

To make patient genotype data on essential pharmacogenes globally available for routine medical care, technologically-simple and intuitive systems are required in order to minimize the need for specialized infrastructure, software, and knowledge. The Medication Safety Code initiative is an effort to improve the ability of clinicians and patients to share pharmacogenetic data and to use it at the point of care.  This system consists of two major components:

* **The Medication Safety Code (MSC), a standardized two-dimensional (2D) barcode** that captures data about genetic variants in essential pharmacogenes for an individual patient. The 2D barcode is based on the Quick Response (QR) code standard, which has become widely popular in recent years because it can be decoded quickly and reliably, has relatively high information density and can encode hyperlinks to pages on the World Wide Web. In addition, the MSC data can also be embedded in electronic health records.
* **A curated knowledge base** that contains information about essential pharmacogenes, including haplotype definitions, phenotype translations and clinical guidelines. This knowledge base is used to automatically interpret the genetic data that is encoded within the 2D barcodes. The knowledge base leverages semantic technologies and ontologies to ensure a high degree of interoperability with other systems.

### Local execution

1. Clone  repository
2. From folder msc3 run:
   + Windows: ```gradlew.bat build```
   + Linux: ```./gradlew build```
3. WAR is located in msc3/build/libs
4. Deploy with Tomcat
5. Open http://localhost:8080/msc3/generate

### Documentation

The documentation for the read-only REST API can be found at http://safety-code.org/msc3/api-docs/.

The documentation for the Java code that runs on the server can be found at http://safety-code.org/msc3/docs/

Further documentation:

+ [Intro to MSC/local execution guide](https://gitlab.com/medication-safety/msc-server/-/blob/master/doc/msc-system.md)
+ [UPGX MSC System](https://gitlab.com/medication-safety/msc-server/-/blob/master/doc/upgx-msc-system.md)
+ [OWL-Server](https://gitlab.com/medication-safety/msc-server/-/blob/master/doc/owl-server.md)

Branches:

+ master: Standard MSC-Card
+ feature-json: Generation of MSC-Card using JSON
+ feature-greek: Standard MSC-Card (Greek hardcoded)
+ feature-upgx-card: Upgx Card (Multiple Sites/Templates)


