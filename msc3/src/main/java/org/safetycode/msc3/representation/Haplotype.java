package org.safetycode.msc3.representation;

import org.safetycode.msc3.util.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sebastian on 15.05.2015.
 */
public class Haplotype implements Marker, Comparable {
    /**
     * Caches the haplotypes.
     */
    private static ArrayList<Haplotype> haplotypes = new ArrayList<Haplotype>();
    /**
     * Caches the haplotypes by their name.
     */
    private static HashMap<String, Haplotype> haplotypesByName = new HashMap<String, Haplotype>();

    /**
     * A unique String identifying the haplotype.
     * These identifiers are also used to evaluate Guideline rules.
     * They must thus not contain periods or other symbols that are not valid for JS variable names.
     * e.g. "CYP2C9_star_2"
     */
    public String id;
    /**
     * The name of the haplotype, human-readable.
     * e.g. "*1"
     */
    public String name;
    /**
     * The gene this haplotype belongs to.
     */
    private Gene gene;
    /**
     * The urlFragment identifying the haplotype.
     */
    private String urlFragment;
    /**
     * Is this the default haplotype?
     */
    private boolean isDefault;


    public Gene getGene() {
        return gene;
    }
    public String getId() { return id; }
    public String getName() { return name; }
    public String getDisplayName() { return name; }
    public String getUrlFragment() {
        return urlFragment;
    }
    public void setUrlFragment(String urlFragment) {
        this.urlFragment = urlFragment;
    }
    public static List<Haplotype> getHaplotypes() { return haplotypes; }
    public static Haplotype getHaplotype(String id) {
        for (Haplotype h : haplotypes)
            if (h.getId().equals(id))
                return h;
        return null;
    }


    /**
     * Constructs a haplotype with the given name for the given gene.
     * @param name name of the haplotype
     * @param gene which gene the haplotype belongs to
     */
    private Haplotype(String id, String name, Gene gene, String urlFragment) {
        this.id = id;
        this.name = name;
        this.gene = gene;
        this.urlFragment = urlFragment;
        this.isDefault = urlFragment.equals("01") ? true : false;

        gene.addHaplotype(this);

        haplotypes.add(this);
        haplotypesByName.put(name, this);
    }

    public String toString() {
        return this.id;
    }

    @Override
    public int compareTo(Object o) {
        String a = this.urlFragment;
        String b = ((Haplotype) o).getUrlFragment();

        for(int i = 0; i < a.length(); i++) {
            int x = Common.BASE_DIGITS.indexOf(a.charAt(i));
            int y = Common.BASE_DIGITS.indexOf(b.charAt(i));

            if (x < y) return -1;
            if (y < x) return 1;
        }

        return 0;
    }

    public static class HaplotypeBuilder {
        private Gene gene;
        private String id;
        private String name;
        private String urlFragment;

        public HaplotypeBuilder setGene(Gene gene) {
            this.gene = gene;
            return this;
        }

        public HaplotypeBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public HaplotypeBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public HaplotypeBuilder setUrlFragment(String urlFragment) {
            this.urlFragment = urlFragment;
            return this;
        }

        public Haplotype build() {
            return new Haplotype(id, name, gene, urlFragment);
        }

    }
}
