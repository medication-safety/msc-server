package org.safetycode.msc3.representation;

import org.safetycode.msc3.util.Common;

import java.util.*;

/**
 * Created by Sebastian on 15.05.2015.
 */
public class Guideline {
    /**
     * Caches guidelines by id.
     */
    private static Map<String, Guideline> guidelinesById = new HashMap<String, Guideline>();
    /**
     * Cache for guidelines.
     */
    private static List<Guideline> guidelines = new ArrayList<Guideline>();
    /**
     * Cache for guidelines with an important modification type.
     */
    private static List<Guideline> importantGuidelines = new ArrayList<Guideline>();

    /**
     * The id of the guideline, which acts as a unique identifier.
     */
    private String name;
    /**
     * The id of the drug affected by the guideline.
     */
    private String drug;
    /**
     * The source of the guideline, e.g. CPIC or DPWG.
     */
    private String source;
    /**
     * The rule that has to be matched for the guideline to apply, based on phenotypes.
     */
    private String phenotypeRule;
    /**
     * The rule that has to be matched for the guideline to apply, based on haplotypes.
     */
    private String diplotypeRule;
    /**
     * The drug dosage recommendation.
     */
    private String recommendation;
    /**
     * What type of modification to the dosage the guideline suggests, e.g. "Important modification".
     */
    private String modificationType;
    /**
     * The date of evidence.
     */
    private String evidenceDate;
    /**
     * The human readable name of the phenotype.
     */
    private String phenotypeDisplayName;
    /**
     * The URL of the guideline website.
     */
    private String evidenceUrl;


    public static List<Guideline> getGuidelines() {
        return guidelines;
    }
    public String getDrug() {
        return drug;
    }
    public String getName() {
        return name;
    }
    public String getSource() {
        return source;
    }
    public String getDiplotypeRule() {
        return diplotypeRule;
    }
    public String getPhenotypeRule() {
        return phenotypeRule;
    }
    public String getRecommendation() {
        return recommendation;
    }
    public String getEvidenceDate() {
        return evidenceDate;
    }
    public String getModificationType() {
        return modificationType;
    }
    public String getPhenotypeDisplayName() {
        return phenotypeDisplayName;
    }
    public String getEvidenceUrl() {
        return evidenceUrl;
    }
    /**
     * The index for ordering, as derived from the id of the rule.
     */
    public int getIndex() {
        return Integer.parseInt(name.substring(name.lastIndexOf("_") + 1, name.length()));
    }
    public static List<Guideline> getImportantGuidelines() {
        return importantGuidelines;
    }

    /**
     * Constructs a guideline with the given id.
     * @param name id of the guideline
     */
    private Guideline(String name,
                      String drug,
                      String source,
                      String phenotypeRule,
                      String diplotypeRule,
                      String recommendation,
                      String modificationType,
                      String evidenceDate,
                      String phenotypeDisplayName,
                      String evidenceUrl) {
        this.name = name;
        this.drug = drug;
        this.source = source;
        this.phenotypeRule = phenotypeRule;
        this.diplotypeRule = diplotypeRule;
        this.recommendation = recommendation;
        this.modificationType = modificationType;
        this.evidenceDate = evidenceDate;
        this.phenotypeDisplayName = phenotypeDisplayName;
        this.evidenceUrl = evidenceUrl;

        guidelines.add(this);
        guidelinesById.put(name, this);
        if (this.modificationType.equals(Common.IMPORTANT_MODIFICATION)) {
            importantGuidelines.add(this);
        }
    }

    /**
     * Sorts the guideline variable by index.
     */
    public static void sortGuidelines() {
        Collections.sort(guidelines, new Comparator<Guideline>() {
            @Override
            public int compare(Guideline a, Guideline b) {
                return a.getIndex() < b.getIndex() ? -1 : 1;
            }
        });
    }

    /**
     * Returns the guideline identified by the id.
     * @param name id of the guideline
     * @return the guideline
     */
    public static Guideline get(String name) {
        return guidelinesById.get(name);
    }

    /**
     * Returns a list of guidelines identified by the names in the list.
     * @param names the names of the guidelines
     * @return a list of guidelines
     */
    public static List<Guideline> get(List<String> names) {
        ArrayList<Guideline> guidelines = new ArrayList<Guideline>();

        for (String name : names) {
            guidelines.add(get(name));
        }

        return guidelines;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public static class GuidelineBuilder {
        private String id;
        private String drug;
        private String source;
        private String phenotypeRule;
        private String diplotypeRule;
        private String recommendation;
        private String modificationType;
        private String evidenceDate;
        private String phenotypeDisplayName;
        private String evidenceUrl;

        public GuidelineBuilder setId(String name) {
            this.id = name;
            return this;
        }

        public GuidelineBuilder setDrug(String drug) {
            this.drug = drug;
            return this;
        }

        public GuidelineBuilder setSource(String source) {
            this.source = source;
            return this;
        }

        public GuidelineBuilder setPhenotypeRule(String phenotypeRule) {
            this.phenotypeRule = phenotypeRule;
            return this;
        }

        public GuidelineBuilder setDiplotypeRule(String diplotypeRule) {
            this.diplotypeRule = diplotypeRule;
            return this;
        }

        public GuidelineBuilder setRecommendation(String recommendation) {
            this.recommendation = recommendation;
            return this;
        }

        public GuidelineBuilder setModificationType(String modificationType) {
            this.modificationType = modificationType;
            return this;
        }

        public GuidelineBuilder setEvidenceDate(String evidenceDate) {
            this.evidenceDate = evidenceDate;
            return this;
        }

        public GuidelineBuilder setPhenotypeDisplayName(String phenotypeDisplayName) {
            this.phenotypeDisplayName = phenotypeDisplayName;
            return this;
        }

        public GuidelineBuilder setEvidenceUrl(String evidenceUrl) {
            this.evidenceUrl = evidenceUrl;
            return this;
        }

        public Guideline build() {
            return new Guideline(id, drug, source, phenotypeRule, diplotypeRule, recommendation, modificationType, evidenceDate, phenotypeDisplayName, evidenceUrl);
        }
    }
}
