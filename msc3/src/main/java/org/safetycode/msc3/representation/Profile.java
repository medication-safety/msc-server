/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.safetycode.msc3.representation;

import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.safetycode.msc3.exceptions.GeneticUrlValidationException;
import org.safetycode.msc3.util.Common;

import javax.imageio.ImageIO;
import javax.script.Bindings;
import javax.script.ScriptException;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.*;

/**
 * Created by Sebastian on 13.05.2015.
 */
public class Profile {
    /**
     * A map of available Markers (Haplotypes, Phenotypes) to their count (as Strings).
     * Since these are the "empty" Markers, all values are "0".
     * We use this to verify guidelines only contain availableMarkers that actually exist in the data.
     */
    private static final Map<String, String> emptyMarkers = getEmptyMarkers(Gene.getGenes());
    /**
     * A list of available Markers (Haplotypes, Phenotypes) for this profile.
     */
    private static final List<String> availableMarkers = getAvailableMarkers(Gene.getGenes());

    /**
     * Is this a valid Profile?
     */
    private boolean isValidFragment;
    /**
     * Markers (phenotypes and haplotypes) that apply to this profile.
     */
    private List<Marker> markers = new ArrayList<Marker>();
    /**
     * Genes that were tested for this profile.
     */
    private List<Gene> genes = new ArrayList<Gene>();
    /**
     * Phenotypes that apply to this profile.
     */
    private List<Phenotype> phenotypes = new ArrayList<Phenotype>();
    /**
     * Haplotypes that apply to this profile.
     */
    private List<Haplotype> haplotypes = new ArrayList<Haplotype>();
    /**
     * The phenotype bindings are used by the JS engine to evaluate if a guideline applies to a profile
     * according to its phenotypes and the phenotype rule of the guideline.
     */
    private Bindings phenotypeBindings;
    /**
     * The haplotype bindings are used by the JS engine to evaluate if a guideline applies to a profile
     * according to its haplotypes and the haplotype rule of the guideline.
     */
    private Bindings haplotypeBindings;
    /**
     * The urlFragment identifying the assay.
     */
    private String assayId;
    /**
     * The urlFragment encoding the genetic test data.
     */
    private String geneticUrlFragment;

    public List<Gene> getGenes() {
        return genes;
    }
    public List<Phenotype> getPhenotypes() {
        return phenotypes;
    }
    public Phenotype getPhenotype(Gene gene) {
        for (Phenotype phenotype : phenotypes) {
            if(phenotype.getGene() == gene) {
                return phenotype;
            }
        }
        return null;
    }
    public List<Haplotype> getHaplotypes() {
        return haplotypes;
    }
    public List<Marker> getMarkers() {
        return this.markers;
    }
    public String getUrl() { return Common.URL_PREFIX + this.assayId + "/" + this.geneticUrlFragment; }
    /**
     * Returns the list of available haplotype names for this profile and the given genes.
     * "Available" includes all possible haplotypes for all given genes.
     * @param genes haplotypes for these genes are included
     * @return the haplotypes
     */
    private static List<String> getAvailableHaplotypeMarkers(List<Gene> genes) {
        List<String> markers = new ArrayList<String>();

        for (Gene gene : genes) {
            for (Haplotype haplotype : gene.getHaplotypes()) {
                markers.add(haplotype.toString());
            }
        }

        return markers;
    }
    /**
     * Returns a map from available haplotype marker names to "0".
     * "Available" includes all possible haplotypes for all given genes.
     * @param genes haplotypes for these genes are included
     * @return the map from marker names to "0"
     */
    private static Map<String, String> getEmptyHaplotypeMarkers(List<Gene> genes) {
        Map<String, String> emptyMarkers = new HashMap<String, String>();

        for (String marker : getAvailableHaplotypeMarkers(genes)) {
            emptyMarkers.put(marker, null);
        }

        return emptyMarkers;
    }
    /**
     * Returns a list of available phenotype marker Strings for this profile and the given genes.
     * "Available" includes all possible phenotypes for all given genes.
     * @param genes the list of genes
     * @return the list of phenotype names
     */
    private static List<String> getAvailablePhenotypeMarkers(List<Gene> genes) {
        List<String> markers = new ArrayList<String>();

        for (Gene gene : genes) {
            for (Phenotype phenotype : gene.getPhenotypes()) {
                markers.add(phenotype.toString());
            }
        }

        return markers;
    }
    /**
     * Returns a map from available phenotype marker names to "0".
     * "Available" includes all possible phenotypes for all given genes.
     * @param genes phenotypes for these genes are included
     * @return the map from marker names to "0"
     */
    private static Map<String,String> getEmptyPhenotypeMarkers(List<Gene> genes) {
        Map<String, String> emptyMarkers = new HashMap<String, String>();

        for (String marker : getAvailablePhenotypeMarkers(genes)) {
            emptyMarkers.put(marker, null);
        }

        return emptyMarkers;
    }
    private static Map<String, String> getEmptyMarkers(List<Gene> genes) {
        Map<String, String> emptyMarkers = new HashMap<String, String>();

        emptyMarkers.putAll(getEmptyHaplotypeMarkers(genes));
        emptyMarkers.putAll(getEmptyPhenotypeMarkers(genes));

        return emptyMarkers;
    }
    private static List<String> getAvailableMarkers(List<Gene> genes) {
        List<String> markers = new ArrayList<String>();

        markers.addAll(getAvailableHaplotypeMarkers(genes));
        markers.addAll(getAvailablePhenotypeMarkers(genes));

        return markers;
    }

    public Bindings getPhenotypeBindings() {
        return phenotypeBindings;
    }

    public Bindings getHaplotypeBindings() {
        return haplotypeBindings;
    }
    /**
     * Returns the list of guidelines that apply to this profile.
     * @return the list of applying guidelines
     */
    public List<Guideline> getGuidelines() {
        List<Guideline> guidelines = new ArrayList<Guideline>();

        for (Guideline guideline : Guideline.getGuidelines()) {
            if (applies(guideline)) {
                guidelines.add(guideline);
            }
        }

        return guidelines;
    }
    /**
     * Returns the list of important guidelines that apply to this profile.
     * @return the list of important applying guidelines
     */
    public List<Guideline> getImportantGuidelines() {
        List<Guideline> importantGuidelines = new ArrayList<Guideline>();

        for (Guideline guideline : Guideline.getImportantGuidelines()) {
            if (applies(guideline)) {
                importantGuidelines.add(guideline);
            }
        }

        return importantGuidelines;
    }

    /**
     * Returns a map from drug names to a list of important guidelines for that drug that apply to this profile.
     * @return the map from drugs to importang guidelines
     */
    public Map<String, List<Guideline>> getImportantGuidelinesByDrug() {
        Map<String, List<Guideline>> importantGuidelines = new HashMap<String, List<Guideline>>();

        for (Guideline guideline : Guideline.getImportantGuidelines()) {
            if (applies(guideline)) {
                String drug = guideline.getDrug();
                if (!importantGuidelines.containsKey(drug)) {
                    importantGuidelines.put(drug, new ArrayList<Guideline>());
                }
                importantGuidelines.get(drug).add(guideline);
            }
        }

        return importantGuidelines;
    }

    /**
     * Returns the list of important guidelines for the given drug that apply to this profile.
     * Important guidelines have a modificationType of "Important modification".
     * @param drug the name of the drug guidelines should be returned for
     * @return the list of important guidelines
     */
    public List<Guideline> getImportantGuidelines(String drug) {
        List<Guideline> importantGuidelines = new ArrayList<Guideline>();

        for(Guideline g : getImportantGuidelines())
            if(g.getDrug().equals(drug))
                importantGuidelines.add(g);

        return importantGuidelines;
    }

    public Profile(String geneticUrlFragment) throws IOException {
        this("00", geneticUrlFragment);
    }

    // TODO: make this method more readable
    /**
     * Constructs a Profile from a genetic data string.
     * @param geneticUrlFragment
     */
    public Profile(String assayId, String geneticUrlFragment) {
        this.assayId = assayId;

        if (geneticUrlFragment.length() % 6 != 0) {
            throw new GeneticUrlValidationException("Invalid URL: the profile identifier length must be a multiple of 6!",
                    new IOException());
        }

        initializeFields(geneticUrlFragment);

        // Process the URL
        for (int i = 0; i < geneticUrlFragment.length(); i += 6) {
            String currentFragment = geneticUrlFragment.substring(i, i+6);
            processFragment(currentFragment, i);
        }
    }

    /**
     * Processes a 6-digit string representing a gene, phenotype and two haplotypes.
     * @param geneticUrlFragment The 6-digit string
     * @param firstIndex the index of the first digit ( = the current gene identifier) within the whole geneticUrl
     * @throws IOException
     */
    private void processFragment(String geneticUrlFragment, int firstIndex) {
        // Get geneticUrlFragment strings
        String geneFragment = geneticUrlFragment.substring(0, 1);
        String phenotypeFragment = geneticUrlFragment.substring(1, 2);
        String firstHaplotypeFragment = geneticUrlFragment.substring(2, 4);
        String secondHaplotypeFragment = geneticUrlFragment.substring(4, 6);

        process(geneticUrlFragment, geneFragment, phenotypeFragment, firstHaplotypeFragment, secondHaplotypeFragment, firstIndex);
    }

    private void process(String wholeBlock,
                         String geneFragment,
                         String phenotypeFragment,
                         String firstHaplotypeFragment,
                         String secondHaplotypeFragment,
                         int firstIndex) {
        // Get corresponding instances
        Gene gene = getGeneFromFragment(geneFragment);
        if (gene == null) {
            throw new GeneticUrlValidationException(
                    "Invalid block " + wholeBlock + "!\n"
                            + "There is no gene associated with identifier \"" + geneFragment
                            + "\" at position " + firstIndex,
                    new IOException());
        }

        Phenotype phenotype = getPhenotypeFromFragment(phenotypeFragment, gene);
        if (phenotype == null) {
            throw new GeneticUrlValidationException(
                    "Invalid block " + wholeBlock + "!\n"
                            + "For gene " + gene.getSymbol()
                            + ", there is no phenotype associated with identifier \"" + phenotypeFragment
                            + "\" at position " + (firstIndex + 1),
                    new IOException());
        }

        Haplotype firstHaplotype = getHaplotypeFromFragment(firstHaplotypeFragment, gene);
        if (firstHaplotype == null) {
            throw new GeneticUrlValidationException(
                    "Invalid block " + wholeBlock + "!\n"
                            + "For gene " + gene.getSymbol()
                            + ", there is no haplotype associated with identifier \"" + firstHaplotypeFragment
                            + "\" at position " + (firstIndex + 2),
                    new IOException());
        }

        Haplotype secondHaplotype = getHaplotypeFromFragment(secondHaplotypeFragment, gene);
        if (secondHaplotype == null) {
            throw new GeneticUrlValidationException(
                    "Invalid block " + wholeBlock + "!\n"
                            + "For gene " + gene.getSymbol()
                            + ", there is no haplotype associated with identifier \"" + secondHaplotypeFragment
                            + "\" at position " + (firstIndex + 4),
                    new IOException());
        }

        this.genes.add(gene);
        this.phenotypes.add(phenotype);
        this.haplotypes.add(firstHaplotype);
        this.haplotypes.add(secondHaplotype);

        // Store instances in markers
        markers.addAll(Arrays.asList(phenotype, firstHaplotype, secondHaplotype));

        // Update bindings (with all possible values for the gene)
        for (Phenotype p2 : gene.getPhenotypes()) {
            if(!p2.getName().equals(phenotype.getName()))
                phenotypeBindings.put(p2.getId(), false);  // Set all possible phenotypes for this gene to false
            else
                phenotypeBindings.put(phenotype.getId(), true);  // Except for the actual phenotype, which is set to true
        }

        // set all haplotype bindings to 0
        for (Haplotype h : gene.getHaplotypes()) {
            haplotypeBindings.put(h.id, 0);
        }
        // increase the actual haplotypes by 1
        haplotypeBindings.put(firstHaplotype.id, Integer.parseInt("" + haplotypeBindings.get(firstHaplotype.id)) + 1);
        haplotypeBindings.put(secondHaplotype.id, Integer.parseInt("" + haplotypeBindings.get(secondHaplotype.id)) + 1);
    }

    private Haplotype getHaplotypeFromFragment(String haplotypeFragment, Gene gene) {
        return gene.getHaplotype(haplotypeFragment);
    }

    private Phenotype getPhenotypeFromFragment(String phenotypeFragment, Gene gene) {
        return gene.getPhenotype(phenotypeFragment);
    }

    private Gene getGeneFromFragment(String geneFragment) {
        return Gene.get(geneFragment);
    }

    /**
     * Returns the Medicine Safety Code image frame.
     * @return Medicine Safety Code image frame
     */
    public BufferedImage getFrameImage() throws IOException {
        return ImageIO.read(new FileInputStream(
                Common.getResource(Common.QR_FRAME).getFile()));
    }

    /**
     * Returns the barcode image for this profile.
     * @return the barcode image
     */
    public BufferedImage getCodeImage() throws CharacterCodingException, UnsupportedEncodingException, WriterException {
        BitMatrix matrix = this.getCodeBitMatrix(this.getCodeData());
        return MatrixToImageWriter.toBufferedImage(matrix);
    }

    /**
     * Returns the BitMatrix for the given data string.
     * @return the BitMatrix
     */
    private BitMatrix getCodeBitMatrix(String data) throws WriterException {
        com.google.zxing.Writer writer = new QRCodeWriter();
        BitMatrix matrix = writer.encode(data, com.google.zxing.BarcodeFormat.QR_CODE, Common.QR_WIDTH, Common.QR_HEIGHT);
        return matrix;
    }


    private String getCodeData() throws UnsupportedEncodingException, CharacterCodingException {
        String charsetName = "ISO-8859-1";
        return new String(this.getCodeBytes(charsetName), charsetName);
    }

    private byte[] getCodeBytes(String charsetName) throws CharacterCodingException {
        Charset charset = Charset.forName(charsetName);
        CharsetEncoder encoder = charset.newEncoder();
        return encoder.encode(CharBuffer.wrap(this.getUrl())).array();
    }

    private void initializeFields(String geneticUrlFragment) {
        this.isValidFragment = isValidFragment(geneticUrlFragment);
        this.geneticUrlFragment = geneticUrlFragment;
        this.phenotypeBindings = Common.JS_ENGINE.createBindings();
        this.haplotypeBindings = Common.JS_ENGINE.createBindings();
    }

    private boolean isValidFragment(String geneticUrlFragment) throws RuntimeException {
        boolean isValid = false;
        if (geneticUrlFragment.length() % 6 == 0) {
            isValid = true;
        }
        else {
            throw new RuntimeException("geneticUrlFragment length is not a multiple of 6!");
        }
        return isValid;
    }

    /**
     * Prints the bindings in phenotypeBindings and haplotypeBindings to stdout.
     */
    public void printBindings() {
        for(String key : phenotypeBindings.keySet()) {
            System.out.println(key + ": " + phenotypeBindings.get(key));
        }
        for(String key : haplotypeBindings.keySet()) {
            System.out.println(key + ": " + haplotypeBindings.get(key));
        }
    }

    /**
     * Returns if the given guideline applies to this profile.
     * @param guideline the guideline to be tested against the profile
     * @return True if the guideline applies, false otherwise
     */
    public boolean applies(Guideline guideline) {
        boolean appliesToPhenotype = false, appliesToDiplotype = false;

        // calculate if the guideline applies to profile
        try {
            appliesToPhenotype = (Boolean) Common.JS_ENGINE.eval(guideline.getPhenotypeRule(), phenotypeBindings);
        }
        catch (ScriptException ex) {
            // do nothing, so appliesToPhenotype stays false
        }

        try {
            appliesToDiplotype = (Boolean) Common.JS_ENGINE.eval(guideline.getDiplotypeRule(), haplotypeBindings);
        } catch (ScriptException e) {
            // do nothing, so appliesToDiplotype stays false
        }


        // calculate if either the phenotype or the haplotypes match a rule
        //boolean applies = appliesToPhenotype || appliesToDiplotype;
        boolean applies = appliesToPhenotype; // but we are only interested if it applies to the phenotype for now

        return applies;
    }

    /**
     * Checks all Guideline instances for errors in the rules (haplotypes & phenotypes).
     * This tests if the rules are syntactically correct & don't throw errors when evaluated by the JSEngine.
     * @throws ScriptException is thrown when an error occurs evaluating any Guideline rules
     */
    public static void testAllGuidelines() throws ScriptException {
        testAllGuidelines(false);
    }

    /**
     * Checks all Guideline instances for errors in the rules (haplotypes & phenotypes).
     * This tests if the rules are syntactically correct & don't throw errors when evaluated by the JSEngine.
     * @param verbose if true, the name of the tested guideline is printed to the console first
     * @throws ScriptException is thrown when an error occurs evaluating any Guideline rules
     */
    public static void testAllGuidelines(Boolean verbose) throws ScriptException {
        for (Guideline guideline : Guideline.getGuidelines()) {
            if (verbose) System.out.println("Testing Guideline " + guideline);
            testGuideline(guideline);
        }
    }

    /**
     * Checks a single guideline for errors in the rules (haplotypes & phenotypes).
     * This tests if the rules are syntactically correct & don't throw errors when evaluated by the JSEngine.
     * @param guideline the guideline to be tested
     * @throws RuntimeException is thrown when an error occurs evaluating the Guideline rules.
     */
    public static void testGuideline(Guideline guideline) throws ScriptException {
        // Create dummy bindings of all available Phenotype & Haplotype markers
        Bindings phenotypeBindings = Common.JS_ENGINE.createBindings();
        Bindings haplotypeBindings = Common.JS_ENGINE.createBindings();

        for (String marker : getAvailablePhenotypeMarkers(Gene.getGenes())) {
            phenotypeBindings.put(marker, false);
        }
        for (String marker : getAvailableHaplotypeMarkers(Gene.getGenes())) {
            haplotypeBindings.put(marker, 0);
        }

        // Test the guideline
        Common.JS_ENGINE.eval(guideline.getPhenotypeRule(), phenotypeBindings);
        Common.JS_ENGINE.eval(guideline.getDiplotypeRule(), haplotypeBindings);
    }
}
