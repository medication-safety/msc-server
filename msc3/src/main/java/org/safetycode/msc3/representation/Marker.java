package org.safetycode.msc3.representation;

/**
 * Created by Sebastian on 22.05.2015.
 */
public interface Marker {
    public Gene getGene();
}
