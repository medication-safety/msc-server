/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Created by Sebastian on 03.08.2016.
 */
package org.safetycode.msc3.mayo;

import java.util.HashMap;
import java.util.Map;
import org.safetycode.msc3.representation.*;
import org.safetycode.msc3.util.Common;

import java.nio.file.Path;
import java.util.List;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.io.File;
import java.util.ArrayList;
import org.safetycode.msc3.representation.MSCManager;

public class URLGenerator {
	MSCManager manager;
	File folder;

	// run with
	// java -cp lib/*:classes org.safetycode.msc3.mayo.URLGenerator <directory_to_process>
	public static void main(String[] args) {
		File folder = null;
		if (args.length > 0)
			folder = new File(args[0]);
		else {
			System.err.println("No arguments given! Use the absolute path to the directory you want to process as the first argument.");
			System.exit(1);
		}

		System.out.println("Running the url generator for " + folder);
		URLGenerator generator = new URLGenerator(folder);
		generator.process();
	}

	public URLGenerator(File folder) {
		this.manager = MSCManager.getInstance();
		this.folder = folder;
	}

	public void process() {
		for (File file : listFilesForFolder(folder)) {
			System.out.println(processFile(file));
		}
	}

	public String processFile(File file) {
		String result = "", firstName = "", lastName = "";
		List<String> lines = new ArrayList<String>();
		try {
			lines = readFile(file);
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}

		for (String line : lines) {
			if (line.startsWith("#")) {
				if (line.startsWith("#patient_First"))
					firstName = line.split(" ")[1];
				if (line.startsWith("#patient_Last"))
					lastName = line.split(" ")[1];
			}
			else {
				String[] fields = line.split("\t");
				result += getUrlFragment(fields);
			}
		}

		return lastName + ", " + firstName + ": " + Common.prefixWithBaseUrl("00/" + result);
	}

	/*
	fields[0] = gene symbol
	fields[1] = phenotype (as id, but without gene prefix)
	fields[2] = haplotype 1 (as id, but without gene prefix)
	fields[3] = haplotype 2 (as id, but without gene prefix)
	*/
	public String getUrlFragment(String[] fields) {
		Map<String, String> urlFragments = new HashMap<String, String>();
		Gene gene = manager.getGene(fields[0]);
		Phenotype phenotype = manager.getPhenotype(Common.toMarkerId(gene.getSymbol(), fields[1]));
		Haplotype haplotype_1 = manager.getHaplotype(Common.toMarkerId(gene.getSymbol(), fields[2]));
		Haplotype haplotype_2 = manager.getHaplotype(Common.toMarkerId(gene.getSymbol(), fields[3]));

		return gene.getUrlFragment() + phenotype.getUrlFragment() + haplotype_1.getUrlFragment() + haplotype_2.getUrlFragment();
	}

	public ArrayList<File> listFilesForFolder(final File folder) {
		ArrayList<File> files = new ArrayList();
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            // do nothing
	        } else {
	            files.add(fileEntry);
	        }
	    }
	    return files;
	}

	List<String> readFile(File file) throws IOException {
	    return Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
  }
}
