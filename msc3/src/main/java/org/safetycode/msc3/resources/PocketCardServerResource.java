/*
 Copyright (C) 2015  Matthias Samwald
 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.safetycode.msc3.resources;
import com.google.zxing.WriterException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.restlet.data.MediaType;
import org.restlet.representation.ByteArrayRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.safetycode.msc3.representation.*;
import org.safetycode.msc3.util.Common;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.safetycode.msc3.util.Common.prettyListString;

/**
 * Created by Sebastian on 20.10.2015.
 */
public class PocketCardServerResource extends ServerResource {
    // attributes
    private String assayId;
    private String profileId;
    private String lab;
    private String name;
    private String birthday;
    private String cardnumber;
    private String date;
    private Profile profile;
    private List<Gene> otherActionableGenes = new ArrayList<Gene>(); // actionable genes that don't fit on the card separately
    private List<Gene> addedActionableGenes = new ArrayList<Gene>(); // keep track of genes added to the card for rollback purposes
    private List<Gene> nonActionableGenes = new ArrayList<Gene>();   // nonActionable genes, printed at the bottom if there is space
    private PdfPTable table;
    // fonts
    private static BaseFont bf;
    private static Font bigBlackFont;
    private static Font blackFont;
    private static Font blackItalicFont;
    private static Font blackBoldItalicFont;
    private static Font smallBlackFont;
    private static Font whiteFont;
    private static Font smallWhiteFont;
    static {
        try {
            bf = BaseFont.createFont("fonts/Calibri.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        bigBlackFont = new Font(bf, 8);
        blackFont = new Font(bf, 7);
        blackItalicFont = new Font(bf, 7, Font.ITALIC);
        blackBoldItalicFont = new Font(bf, 7, Font.BOLDITALIC);
        smallBlackFont = new Font(bf, 6);
        whiteFont = new Font(bf, 8, Font.NORMAL, new BaseColor(255, 255, 255));
        smallWhiteFont = new Font(bf, 6, Font.NORMAL, new BaseColor(255, 255, 255));
    }

    @Override
    protected void doInit() throws ResourceException {
        // process route
        this.assayId = getAttribute("assayId");
        this.profileId = getAttribute("profileId");
        // process query parameters
        this.lab = getQueryValue("lab", "");
        this.name = getQueryValue("name", "");
        this.birthday = getQueryValue("birthday", "");
        this.cardnumber = getQueryValue("cardnumber", "");
        // add automatic values
        this.date = new SimpleDateFormat("dd.MM.yyyy").format(new Date()); // current date
        this.profile = new Profile(assayId, profileId);
        try {
            this.table = getTableTemplate();
        }
        catch(DocumentException e) {
            System.err.println(e.getMessage());
        }
        // Debug output
        System.out.println("Creating profile " + this.profileId);
        System.out.println("Important guidelines: " + profile.getImportantGuidelines());
    }

    /**
     * A wrapper for getQueryValue that returns a default value if the value would be null.
     *
     * @param name         Name of the attribute
     * @param defaultValue Default value
     * @return The value of the attribute or the default value, if it would be null
     */
    private String getQueryValue(String name, String defaultValue) {
        String value = getQueryValue(name);
        if (value == null) {
            return defaultValue;
        }
        return value;
    }

    /**
     * General settings for the textfields
     */
    private TextField getTextFieldTemplate(PdfWriter writer, Rectangle rect, String name) {
        TextField textField = new TextField(writer, rect, name);
        if (name=="CardnumberField")
        {
            textField.setBackgroundColor(new BaseColor(76, 139, 179));
            textField.setFontSize(6);
            textField.setTextColor(new BaseColor(255, 255, 255));
        }
        else
        {
            textField.setBackgroundColor(new BaseColor(254, 254, 254));
            textField.setFontSize(8);
        }
        textField.setFont(bf);
        textField.setBorderWidth(0f);
        textField.setOptions(TextField.REQUIRED);

        return textField;
    }

    private void addTextField(PdfStamper stamper, TextField textField, int page) {
        try {
            stamper.addAnnotation(textField.getTextField(), page);
        } catch (IOException e) {
            throw new ExceptionConverter(e);
        } catch (DocumentException e) {
            throw new ExceptionConverter(e);
        }
    }

    @Get("pdf")
    public Representation getPdf() throws IOException, DocumentException, WriterException {
        PdfReader reader = new PdfReader(Common.POCKET_CARD_TEMPLATE);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfStamper stamper = new PdfStamper(reader, baos);
        printAdditionalInformation(stamper);
        printCodeImage(stamper);
        printTable(stamper);
        stamper.close();
        return new ByteArrayRepresentation(baos.toByteArray(), MediaType.APPLICATION_PDF);
    }

    /**
     * Prints the QR-Code on the frontsite of the MSC-Card
     */
    private void printCodeImage(PdfStamper stamper) throws IOException, DocumentException, WriterException {
        PdfContentByte canvas = stamper.getOverContent(1);
        addCodeImage(canvas);
    }

    /**
     * Defines the position and the scale of the QR-Code
     */
    private void addCodeImage(PdfContentByte canvas) throws DocumentException, IOException, WriterException {
        Image codeImage = Image.getInstance(canvas, this.profile.getCodeImage(), 1f);
        codeImage.setAbsolutePosition(10f, 58f);
        codeImage.scalePercent(53f);
        canvas.addImage(codeImage);
    }

    /**
     * Adds information to the front page of the pocket card.
     *
     * @param stamper The stamper for the pdf that will be modified
     * @throws UnsupportedEncodingException
     */
    private void printAdditionalInformation(PdfStamper stamper) throws UnsupportedEncodingException {
        // write front page information
        PdfContentByte canvas = stamper.getOverContent(1);
        printLaboratory(canvas);
        // write back page information
        canvas = stamper.getOverContent(2);
        printDate(canvas);
        printCardNumber(stamper);
        printName(stamper);
        printBirthday(stamper);
    }

    /**
     * Prints the contact information on the frontsite of the card
     */
    private void printLaboratory(PdfContentByte canvas) throws UnsupportedEncodingException {
        printWhitePhrase(canvas, this.lab, 4, 41);
    }

    /**
     * Prints the name on the backsite of the card
     * If name attribute is empty, a textfield will be generate
     */
    private void printName(PdfStamper stamper) throws UnsupportedEncodingException {
        String name = String.format("Name: %s", this.name);

        printBigBlackPhrase(stamper.getOverContent(2), name, 116, 143);

        if (this.name == "") {
            printNameField(stamper);
        }
    }

    /**
     * Generates a textfield for the attribute name
     */
    private void printNameField(PdfStamper stamper) {
        TextField nameField = getTextFieldTemplate(stamper.getWriter(),
                new Rectangle(138, 141, 238, 150), "NameField");
        addTextField(stamper, nameField, 2);
    }

    private void printBirthday(PdfStamper stamper) throws UnsupportedEncodingException {
        String birthday = String.format("Date of birth: %s", this.birthday);

        printBigBlackPhrase(stamper.getOverContent(2), birthday, 116, 133);

        if (this.birthday == "") {
            printBirthdayField(stamper);
        }
    }

    private void printBirthdayField(PdfStamper stamper) {
        TextField birthdayField = getTextFieldTemplate(stamper.getWriter(),
                new Rectangle(160, 131, 238, 140), "BirthdayField");
        addTextField(stamper, birthdayField, 2);
    }


    private void printCardNumber(PdfStamper stamper) throws UnsupportedEncodingException {
        String cardnumber = String.format("Card number: %s", this.cardnumber);

        printSmallWhitePhrase(stamper.getOverContent(2),cardnumber , 177, 4);

        if (this.cardnumber == "") {
            printCardnumberField(stamper);
        }
    }

    private void printCardnumberField(PdfStamper stamper) {
        TextField cardnumberField = getTextFieldTemplate(stamper.getWriter(),
                new Rectangle(212, 9, 242, 3), "CardnumberField");
        addTextField(stamper, cardnumberField, 2);
    }

    private void printDate(PdfContentByte canvas) throws UnsupportedEncodingException {
        printSmallWhitePhrase(canvas, String.format("Date printed: %s", this.date), 4, 4);
    }

    /**
     * Adds the medication table to the back of the pocket card.
     *
     * @param stamper The stamper for the pdf that will be modified
     * @throws DocumentException
     */
    private void printTable(PdfStamper stamper) throws DocumentException {
        List<Gene> genes = profile.getGenes();
        //a counter to delete the last border line
        int counterlastElement=0;
        boolean lastElement;
        // prepare pdf printing
        PdfContentByte canvas = stamper.getOverContent(2);
        // add non-default phenotype genes
        for (Gene gene : genes) {
            if (counterlastElement==genes.size()-1 && nonActionableGenes.size()==0) {
                lastElement=true;

                addRow(gene, lastElement);
            } else {
                lastElement=false;
                addRow(gene, lastElement);
                counterlastElement++;
            }
        }
        // add "No guidelines apply" if applicable
        if (genes.size() == nonActionableGenes.size()) {
            table.addCell(makeCell());
            table.addCell(makeCell("No pharmacogenomic guidelines apply to this patient.", blackBoldItalicFont));
        }
        // add "Other actionable genotypes" row if applicable
        else if (otherActionableGenes.size() > 0) {
            addOtherActionableGenes();
        }
        // add "Other genes tested" row if applicable
        else {
            addNonActionableGenes();
        }
        // write the table
        table.writeSelectedRows(0, -1, 5, 130, canvas);
    }

    /**
     * Adds a gene row to the table that will be displayed at the back of the pocket card.
     * Returns true if the row was added to the table, or false if the row was not added and the gene
     * should be displayed in the "Other genes tested" row instead.
     *
     * @param gene                 The gene
     * @param nonactionableGenes   List of not actionable genes so far
     * @param otherActionableGenes List of actionable genes that don't fit on the card separately so far
     */
    private void addRow(Gene gene, boolean lastElement) {
        Phenotype phenotype = getPhenotype(gene);
        List<String> drugs = phenotype.getAffectedDrugs();
        if (drugs.size() == 0) { // skip if there is no affected medication
            nonActionableGenes.add(gene);
            return;
        }
        // only print the row if an important guideline applies for that gene
        for (Guideline guideline : profile.getImportantGuidelines()) {
            if (this.profile.applies(guideline)) {
                addRecommendationRow( gene, phenotype, prettyListString(drugs), lastElement);
                if (table.getTotalHeight() > Common.POCKET_CARD_MAX_TABLE_HEIGHT) { // don't print the gene, but keep track and print it later
                    table.deleteLastRow();
                    otherActionableGenes.add(gene);
                    addedActionableGenes.remove(addedActionableGenes.size() - 1);
                }
                return;
            }
        }
        // otherwise add it to the list of nonactionable genes
        nonActionableGenes.add(gene);
    }

    /**
     * If the maximum height of the table has reached and more actionable genes are avaible, the last row will be deleted.
     * Instead the actionable genes are summarized in one string.
     * If the size of summarized string is less then 4, each actionable gene will diplayed with their phenotype.
     * If necessary, two rows will be deleted if maximum height has reached.
     */
    private void addOtherActionableGenes() {
        if (otherActionableGenes.size() > 0) {
            // left cell
            PdfPCell cell = makeBottomCell();
            cell.setPaddingTop(-2); // display bugfix
            cell.addElement(new Phrase("Other actionable genes", smallBlackFont));
            table.addCell(cell);
            // right cell
            cell = makeBottomCell();
            cell.setPaddingTop(0f);
            List<String> geneStrings = new ArrayList<String>();
            Phrase phrase=new Phrase(8f);
            if(otherActionableGenes.size() < 4) {
                for (Gene gene : otherActionableGenes) {
                    phrase.add(new Chunk(gene.getSymbol(),blackItalicFont));
                    phrase.add(new Chunk(" (" + getPhenotype(gene).getDisplayName() + "), ",blackFont));
                }
            }
            else{
                phrase.add(new Chunk(prettyListString(otherActionableGenes),blackItalicFont));
            }
            phrase.add(new Chunk("\nVisit website or scan QR Code for further guidance!", blackItalicFont));
            cell.addElement(phrase);
            table.addCell(cell);
        }

        // rollback if necessary
        if (table.getTotalHeight() > Common.POCKET_CARD_MAX_TABLE_HEIGHT) {
            table.deleteLastRow(); // delete the row that was just added
            table.deleteLastRow(); // delete the last actionable gene row
            Gene lastGene = addedActionableGenes.get(addedActionableGenes.size() - 1);
            addedActionableGenes.remove(addedActionableGenes.size() - 1);
            otherActionableGenes.add(lastGene);

            addOtherActionableGenes(); // try again
        }
    }

    /**
     * Prints the last row with information about genes with non-actionable phenotypes.
     * If the maximum table height is reached, it will not displayed.
     *
     * @param otherGenes
     * @param table
     */
    private void addNonActionableGenes() {
        if (nonActionableGenes.size() > 0) {
            PdfPCell cell = makeBottomCell();
            cell.setPaddingTop(-2f); // display bugfix
            cell.addElement(new Phrase("Other genes tested", blackFont));
            cell.addElement(new Phrase("Not actionable", smallBlackFont));
            table.addCell(cell);
            table.addCell(makeBottomCell(prettyListString(nonActionableGenes),blackItalicFont));
        }
        // rollback if necessary
        if (table.getTotalHeight() > Common.POCKET_CARD_MAX_TABLE_HEIGHT) {
            table.deleteLastRow(); // delete the row that was just added
            table.deleteLastRow(); // delete the last actionable gene row
            Gene lastGene = addedActionableGenes.get(addedActionableGenes.size() - 1);
            addedActionableGenes.remove(addedActionableGenes.size() - 1);
           // otherActionableGenes.add(lastGene);
            boolean lastElement=true;
            addRow(lastGene, lastElement);
           // addOtherActionableGenes(); // print the actionable genes row instead now
        }
    }

    /**
     * A PDFTable instance with the right headers, style and size for the table on the back of the pocket card.
     *
     * @return The PdfTable
     * @throws DocumentException
     */
    private PdfPTable getTableTemplate() throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        table.setTotalWidth(Common.CARD_COLUMN_WIDTH);
        table.setWidths(Common.CARD_COLUMN_WIDTHS);
        table.addCell(makeCell("Gene, status", blackItalicFont));
        table.addCell(makeCell("Critical drug substances (modification recommended!)", blackItalicFont));
        return table;
    }

    /**
     * Defines the general settings of a row
     */
    private void addRecommendationRow( Gene gene, Phenotype phenotype, String drugs, boolean lastElement) {
        PdfPCell cell;
            if(lastElement==true) {
                cell = makeBottomCell();
            }
            else {
                cell = makeCell();
            }
        cell.setPaddingTop(-2); // display bugfix
        cell.addElement(new Phrase(gene.getSymbol(), blackItalicFont));
        cell.addElement(new Phrase(phenotype.getDisplayName(), smallBlackFont));
        table.addCell(cell);
        if(lastElement==true) {
            table.addCell(makeBottomCell(drugs));
        }
        else {
            table.addCell(makeCell(drugs));
        }
        addedActionableGenes.add(gene);
    }

    /**
     * Returns the phenotype of this profile for a given gene.
     *
     * @param gene
     * @return
     */
    private Phenotype getPhenotype(Gene gene) {
        Phenotype phenotype = null;
        for (Marker marker : this.profile.getMarkers()) {
            if (marker.getGene() == gene) {
                if (marker.getClass() == Phenotype.class) {
                    phenotype = (Phenotype) marker;
                }
            }
        }
        return phenotype;
    }

    /**
     * Returns the haplotypes of this profile for a given gene.
     *
     * @param gene
     * @return
     */
    private List<Haplotype> getHaplotypes(Gene gene) {
        List<Haplotype> haplotypes = new ArrayList<Haplotype>();
        for (Marker marker : this.profile.getMarkers()) {
            if (marker.getGene() == gene) {
                if (marker.getClass() == Haplotype.class) {
                    haplotypes.add((Haplotype) marker);
                }
            }
        }
        return haplotypes;
    }

    /**
     * Adds a phrase to the canvas. Supports multiline Strings.
     *
     * @param canvas The target canvas
     * @param text   The text to be displayed
     * @param font   The font to be used
     * @param x      The x coordinate
     * @param y      The y coordinate
     */
    private void printPhrase(PdfContentByte canvas, String text, Font font, int x, int y) throws UnsupportedEncodingException {
        printPhrase(canvas, text, font, Element.ALIGN_LEFT, x, y);
    }

    /**
     * Adds a phrase to the canvas. Supports multiline Strings.
     *
     * @param canvas    The target canvas
     * @param text      The text to be displayed
     * @param font      The font to be used
     * @param alignment The alignment to be used
     * @param x         The x coordinate
     * @param y         The y coordinate
     * @throws UnsupportedEncodingException
     */
    private void printPhrase(PdfContentByte canvas, String text, Font font, int alignment, int x, int y) throws UnsupportedEncodingException {
        int offset = 0;
        for (String line : text.split("\n")) {
            ColumnText.showTextAligned(canvas, alignment, new Phrase(line, font), x, y + offset, 0);
            offset -= font.getCalculatedSize() + Common.LAB_NEWLINE_SPACING;
        }
    }

    private void printWhitePhrase(PdfContentByte canvas, String text, int x, int y) throws UnsupportedEncodingException {
        printPhrase(canvas, text, whiteFont, x, y);
    }

    private void printSmallWhitePhrase(PdfContentByte canvas, String text, int x, int y) throws UnsupportedEncodingException {
        printSmallWhitePhrase(canvas, text, Element.ALIGN_LEFT, x, y);
    }

    private void printSmallWhitePhrase(PdfContentByte canvas, String text, int alignment, int x, int y) throws UnsupportedEncodingException {
        printPhrase(canvas, text, smallWhiteFont, alignment, x, y);
    }

    private void printBigBlackPhrase(PdfContentByte canvas, String text, int x, int y) throws UnsupportedEncodingException {
        printBigBlackPhrase(canvas, text, Element.ALIGN_LEFT, x, y);
    }

    private void printBigBlackPhrase(PdfContentByte canvas, String text, int alignment, int x, int y) throws UnsupportedEncodingException {
        printPhrase(canvas, text, bigBlackFont, alignment, x, y);
    }

    private void addBlackPhrase(PdfContentByte canvas, String text, int x, int y) throws UnsupportedEncodingException {
        addBlackPhrase(canvas, text, Element.ALIGN_LEFT, x, y);
    }

    private void addBlackPhrase(PdfContentByte canvas, String text, int alignment, int x, int y) throws UnsupportedEncodingException {
        printPhrase(canvas, text, blackFont, alignment, x, y);
    }

    private PdfPCell makeCell(String text) {
        return makeCell(text, blackFont);
    }

    private PdfPCell makeBottomCell(String text) {
        return styleCell(new PdfPCell(new Phrase(text, blackFont)), true);
    }

    private PdfPCell makeCell(String text, Font font) {
        return styleCell(new PdfPCell(new Phrase(text, font)));
    }

    private PdfPCell makeBottomCell(String text, Font font) {
        return styleCell(new PdfPCell(new Phrase(text, font)), true);
    }

    private PdfPCell makeCell() {
        return styleCell(new PdfPCell());
    }

    private PdfPCell makeBottomCell() {
        return styleCell(new PdfPCell(), true);
    }

    private PdfPCell styleCell(PdfPCell cell) {
        return styleCell(cell, false);
    }

    private PdfPCell styleCell(PdfPCell cell, Boolean bottom) {
        if (bottom) {
            // bottom cells have no border
            cell.setBorder(Rectangle.NO_BORDER);
        } else {
            // non-bottom cells have a border at the bottom
            cell.setBorder(Rectangle.BOTTOM);
        }
        cell.setBorderColor(Common.POCKET_CARD_TABLE_BORDER_COLOR);
        cell.setBorderWidth(0.4f);
        return cell;
    }
}