/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.safetycode.msc3.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Sebastian on 27.07.2015.
 */
public class StringReader {
	 public String readFile(String fileName) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String separator = System.getProperty("line.separator");
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line).append(separator);
		}
		return stringBuilder.toString();
	}
}
