/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.safetycode.msc3.populate;

import org.safetycode.msc3.representation.Guideline;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Sebastian on 16.06.2015.
 */
public class GuidelinePopulator extends Populator {
    private Properties properties;
    private Map<String, Guideline.GuidelineBuilder> builders = new HashMap<String, Guideline.GuidelineBuilder>();

    public GuidelinePopulator(String path) throws IOException {
        this.properties = readProperties(path);
    }

    @Override
    public void populate() throws IOException {
        prepareGuidelines();
        createGuidelines();
        Guideline.sortGuidelines();
    }

    private void prepareGuidelines() throws IOException {
        Enumeration e = this.properties.propertyNames();
        while(e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String value = this.properties.getProperty(key);
            prepareGuideline(key, value);
        }
    }

    private void prepareGuideline(String key, String value) throws IOException {
        String[] tokens = key.split("\\.");
        String guideline = tokens[0];
        String property = tokens[1];

        Guideline.GuidelineBuilder builder = getBuilder(guideline);
        addProperty(getBuilder(guideline), property, value);

    }

    private void addProperty(Guideline.GuidelineBuilder builder, String property, String value) throws IOException {
        if (property.equals("drug")) {
            builder.setDrug(value);
        }
        else if (property.equals("source")) {
            builder.setSource(value);
        }
        else if (property.equals("phenotype_rule")) {
            builder.setPhenotypeRule(value);
        }
        else if (property.equals("diplotype_rule")) {
            builder.setDiplotypeRule(value);
        }
        else if (property.equals("recommendation")) {
            builder.setRecommendation(value);
        }
        else if (property.equals("modification_type")) {
            builder.setModificationType(value);
        }
        else if (property.equals("evidence_date")) {
            builder.setEvidenceDate(value);
        }
        else if (property.equals("phenotype_display_name")) {
            builder.setPhenotypeDisplayName(value);
        }
        else if (property.equals("url")) {
            builder.setEvidenceUrl(value);
        }
        else {
            throw new IOException("Found invalid property name: " + property);
        }
    }

    private Guideline.GuidelineBuilder getBuilder(String key) {
        if (builders.containsKey(key)) {
            return builders.get(key);
        } else {
            Guideline.GuidelineBuilder builder = new Guideline.GuidelineBuilder()
                    .setId(key);
            builders.put(key, builder);
            return builder;
        }
    }

    private void createGuidelines() {
        for (Guideline.GuidelineBuilder builder : this.builders.values()) {
            builder.build();
        }
    }
}
