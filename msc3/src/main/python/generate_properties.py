import os
from openpyxl import load_workbook
import re 
import shutil, errno
import xml.etree.ElementTree as ET

def log(func):
	def wrapper(*args, **kwargs):
		print("Generating {} from {}...".format(os.path.abspath(args[1]), 
												os.path.abspath(args[0])))
		result = func(*args, **kwargs)
		print("Done!")
		return result
	return wrapper


def copyanything(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc: # python >2.5
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else: raise


def sorted_nicely(l): 
    """ Sort the given iterable in the way that humans expect.""" 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)


def get_named_range_values(wb, range_string):
	"""
	Reads a named range from the workbook and returns the list of values.
	"""
	try:
		named_range = wb.get_named_range(range_string)
		destination = list(named_range.destinations)[0]
		if destination[0] is None:  # no idea why this happens, but it does...
			sheet = wb.get_sheet_by_name(named_range.attr_text.split('!')[0].replace("'", ""))  # so bugfix ftw
		else:
			sheet = wb.get_sheet_by_name(destination[0]) # the worksheet object
		address = destination[1].replace('$', '')        # the address, e.g. "B2:B24"
		return [str(tup[0].value) for tup in sheet[address]]
	except:
		print("Problem with named range: " + range_string)
		raise


def get_values_as_property_keys(wb, namedrange):
	return [to_property_key(x) for x in get_named_range_values(wb, namedrange)]


def get_values_as_property_values(wb, namedrange):
	return [to_property_value(x) for x in get_named_range_values(wb, namedrange)]


def set_named_range_values(wb, range, values):
	"""
	Sets the values in a named range to the specified list.
	"""
	namedrange = wb.get_named_range(range)
	sheet = namedrange.destinations[0][0]
	start, end = namedrange.destinations[0][1].replace('$', '').split(':')
	for i, cell in enumerate(sheet[start:end]):
		cell[0].value = values[i]


def to_property_key(text):
	replacements = {' ': '_',
					':': '\\:',
					'(': '',
					')': '',
					':': '',
					'*': 'star_',
					'#': 'hash_'}

	text = text.strip()
	for old, new in replacements.items():
		text = text.replace(old, new)

	return text


def to_property_value(text):
	# return empty string for NoneType
	if type(text) is None:
		return ""
	# newlines have to be escaped
	return text.replace('\n', '\\\n')


def write_property(out, key, value, keyjust = lambda x: x.ljust(40)):
	"""
	A helper function to write justified key-value-pairs to a property file.
	"""
	out.write(keyjust(key) + '= ' + value + '\n')


def generate_genes_properties(in_file, out_file):
	# get values from xlsx file
	wb = load_workbook(in_file, data_only=True)
	get_values = lambda namedrange: get_named_range_values(wb, namedrange)

	genes = get_values_as_property_keys(wb, 'genes')
	url_fragments = get_values_as_property_values(wb, 'url_fragments')

	# create phenotype-haplotype-substitions, used by generate_guidelines_properties
	substitutions = {phenotype: haplotype for phenotype, haplotype
										  in zip(get_values('phenotypes'), get_values('haplotypes'))
										  if haplotype != 'None'} # yup, we are checking strings
	
	# print properties file
	with open(out_file, 'w') as output:
		for i, gene in enumerate(genes):
			write_property(output, gene, url_fragments[i], keyjust = lambda x: x.ljust(10))

	return genes, substitutions


@log
def generate_haplotypes_properties(in_file, out_file, genes):

	def read_gene_sheet(wb, gene, property_syntax=True):
		"""
		Reads a workbook sheet describing the haplotypes of a gene.
		The name of the sheet must be the same as the name of the gene.
		If property_syntax is true, haplotype names are transformed to valid property keys.
		Otherwise, haplotype names are returned as they occur in the workbook.
		Returns a map of haplotype property keys to values.
		"""
		if not gene in wb.get_sheet_names():
			print("Gene sheet for {} does not exist!".format(gene))
			return {}

		sheet = wb.get_sheet_by_name(gene)
		properties = {}
		columns = ["urlfragment", "haplotype", "full haplotype"]
		column_indices = {}

		for i, row in enumerate(sheet.rows):
			# header line
			if i == 0:
				for j, cell in enumerate(row):
					for column in columns:
						if cell.value == column:
							column_indices[column] = j
			# body
			else:				
				haplotype_id = str(row[column_indices["haplotype"]].value)
				url_fragment = str(row[column_indices["urlfragment"]].value)
				pretty_name = str(row[column_indices["full haplotype"]].value)

				if property_syntax:		# transform haplotype names?
					haplotype_id = to_property_key(haplotype_id)

				properties[".".join([gene, haplotype_id, "urlFragment"])] = url_fragment
				properties[".".join([gene, haplotype_id, "name"])] = pretty_name

		return properties


	# get values from xlsx file
	wb = load_workbook(in_file, data_only=True)

	properties = dict()
	for gene in genes:
		# update properties with the property dict for every gene
		properties.update(read_gene_sheet(wb, gene))
		# add default unknown haplotype
		properties[gene + ".haplotype_0.name"] = "Unknown haplotype"
		properties[gene + ".haplotype_0.urlFragment"] = "00"

	# print properties file
	with open(out_file, 'w') as output:
		last_gene = None
		for k in sorted_nicely(properties):
			gene = k.split(".")[0]
			if last_gene is None or gene != last_gene:
				# print heading
				output.write("\n#{}\n".format(gene))

			# print property pair
			write_property(output, k, properties[k])
			last_gene = k.split(".")[0]


@log
def generate_phenotypes_properties(in_file, out_file, genes):

	def read_phenotypes(wb):
		"""
		Returns a map of phenotype property keys to values.
		"""

		def to_phenotype_id(phenotype):
			"""
			Replaces the first "_" in a phenotype with a ".".
			"""
			return phenotype.replace("_", ".", 1)

		sheet = wb.get_sheet_by_name("Phenotypes")
		properties = {}
		columns = ["Phenotype", "Lookup value", "Display name"]
		column_indices = {}

		for i, row in enumerate(sheet.rows):
			# header line: get indices of the interesting columns
			if i == 0:
				for j, cell in enumerate(row):
					for column in columns:
						if cell.value == column:
							column_indices[column] = j
			# body
			else:
				phenotype_id = to_phenotype_id(str(row[column_indices["Phenotype"]].value))
				if " " in phenotype_id or "&" in phenotype_id or "|" in phenotype_id:
					continue	# skip composite phenotypes
				url_fragment = phenotype_id.split("_")[-1]
				pretty_name = str(row[column_indices["Lookup value"]].value)
				display_name = str(row[column_indices["Display name"]].value)

				properties[".".join([phenotype_id, "urlFragment"])] = url_fragment
				properties[".".join([phenotype_id, "name"])] = pretty_name
				properties[".".join([phenotype_id, "display"])] = display_name

		return properties


	# get values from xlsx file
	wb = load_workbook(in_file, data_only=True)

	properties = read_phenotypes(wb)	# read phenotypes from xls-file
	for gene in genes:					# add default phenotypes
		properties[gene + ".phenotype_0.display"] = "Wild-type phenotype"
		properties[gene + ".phenotype_0.name"] = "Wild-type phenotype"
		properties[gene + ".phenotype_0.urlFragment"] = "0"

	# print properties file
	with open(out_file, 'w') as output:
		last_gene = None
		for k in sorted_nicely(properties):
			gene = k.split(".")[0]
			if last_gene is None or gene != last_gene:
				# print heading for this gene
				output.write("\n# {}\n".format(gene))

			# print property pair
			write_property(output, k, properties[k])
			last_gene = k.split(".")[0]


@log
def generate_guidelines_properties(in_file, out_file, substitutions):
	"""
	Generates a property file from an excel file with guidelines.
	"""

	def substitute_all(text, substitutions):
		for old, new in substitutions.items():
			text = text.replace(old, new)

		return text

	with open(out_file, 'w') as output:
		# get values from xlsx file
		wb = load_workbook(in_file, data_only=True)

		get_values = lambda namedrange: get_named_range_values(wb, namedrange)

		names = get_values('names')
		rule_statuses = get_values('rule_statuses')
		drugs = get_values('drugs')
		sources = get_values('sources')
		phenotype_rules = get_values('phenotype_rules')
		recommendations = get_values('recommendations')
		modification_types = get_values('modification_types')
		# generate diplotype_rules from phenotype_rules via substitution
		diplotype_rules = list(map(lambda rule: substitute_all(rule, substitutions), phenotype_rules))
		evidence_dates = get_values('evidence_dates')
		urls = list(map(lambda url: url.split()[0], get_values('urls')))
		phenotype_display_names = get_values('phenotype_lookup_values')

		# write properties file
		for i, name in enumerate(names):
			if rule_statuses[i] == "disabled":
				continue

			name = to_property_key(name)
			drug = to_property_value(drugs[i])
			source = to_property_value(sources[i])
			phenotype_rule = to_property_value(phenotype_rules[i])
			diplotype_rule = to_property_value(diplotype_rules[i])
			recommendation = to_property_value(recommendations[i])
			modification_type = to_property_value(modification_types[i])
			evidence_date = to_property_value(evidence_dates[i])
			url = to_property_value(urls[i])
			phenotype_display_name = to_property_value(phenotype_display_names[i])

			write_property(output, '{}.drug'.format(name), drug)
			write_property(output, '{}.source'.format(name), source)
			write_property(output, '{}.phenotype_rule'.format(name), phenotype_rule)
			write_property(output, '{}.diplotype_rule'.format(name), diplotype_rule)
			write_property(output, '{}.recommendation'.format(name), recommendation)
			write_property(output, '{}.modification_type'.format(name), modification_type)
			write_property(output, '{}.evidence_date'.format(name), evidence_date)
			write_property(output, '{}.url'.format(name), url)
			write_property(output, '{}.phenotype_display_name'.format(name), phenotype_display_name)
			output.write('\n')
	
		return set(drugs)

# available_drugs are drug names for which pharmacogenomic guidelines exist
@log
def generate_drugs_properties(in_file, out_file, available_drugs):
	ns = {'db': 'http://www.drugbank.ca'}
	tree = ET.parse(in_file)
	root = tree.getroot()
	drug_to_products = {}
	for drug in root.findall("db:drug", ns):
		name = to_property_key(drug.find("db:name", ns).text)
		if name in available_drugs:
			products = sorted(list(set([name]) | set(map(
				lambda x: x.find("db:name", ns).text,
				drug.findall("db:products/db:product", ns)))))
			drug_to_products[name] = products

	keyjust = lambda x: x.ljust(max([len(drug) for drug in drug_to_products]) + 1)
	with open(out_file, 'w') as output:
		for drug in sorted(drug_to_products):
			value = ' '.join(drug_to_products[drug])
			write_property(output, drug, value, keyjust = keyjust)


def generate_properties(config):
	genes, substitutions = generate_genes_properties(config["in"]["markers"], config["out"]["genes"])
	generate_haplotypes_properties(config["in"]["markers"], config["out"]["haplotypes"], genes)
	generate_phenotypes_properties(config["in"]["markers"], config["out"]["phenotypes"], genes)
	drugs = generate_guidelines_properties(config["in"]["guidelines"], config["out"]["guidelines"], substitutions)
	# the generation of drug synonyms depends on a big xml file that is not part of the repository,
	# so the next line is commented out by default
	#generate_drugs_properties(config["in"]["drugs"], config["out"]["drugs"], drugs)


config_dir = os.path.join("..", "resources", "config")
excel_dir = os.path.join("..", "excel")

config = {"in": {
			"markers":		os.path.join(excel_dir, "markers.xlsx"),
			"guidelines": 	os.path.join(excel_dir, "guidelines.xlsx"),
			"drugs":		"drugbank.xml"
			},
		  "out": {
		  	"genes":		os.path.join(config_dir, "genes.properties"),
		  	"haplotypes":	os.path.join(config_dir, "haplotypes.properties"),
		  	"phenotypes":	os.path.join(config_dir, "phenotypes.properties"),
		  	"guidelines":	os.path.join(config_dir, "guidelines.properties"),
		  	"drugs":		os.path.join(config_dir, "drugs.properties")
		  	}
		 }

# generate property files
generate_properties(config)
