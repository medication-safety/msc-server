package RepresentationTests

import org.safetycode.msc3.representation.Assay
import org.safetycode.msc3.representation.Gene
import org.safetycode.msc3.representation.Guideline
import org.safetycode.msc3.representation.Haplotype
import org.safetycode.msc3.representation.MSCManager
import org.safetycode.msc3.representation.Profile
import spock.lang.Shared
import spock.lang.Specification

import javax.script.ScriptException

/**
 * Created by Sebastian on 15.05.2015.
 */
class ManagerTests extends Specification{

    MSCManager manager = MSCManager.getInstance()

    def "the GuidelineManager is initialized"() {
        expect:
        manager.getGuidelineManager() != null
        manager.getGuidelineManager().class.equals(MSCManager.GuidelineManager.class)
    }

    def "the GeneManager is initialized"() {
        expect:
        manager.getGeneManager() != null
        manager.getGeneManager().class.equals(MSCManager.GeneManager.class)
    }

    def "the HaplotypeManager is initialized"() {
        expect:
        manager.getHaplotypeManager() != null
        manager.getHaplotypeManager().class.equals(MSCManager.HaplotypeManager.class)
    }

    def "the PhenotypeManager is initialized"() {
        expect:
        manager.getPhenotypeManager() != null
        manager.getPhenotypeManager().class.equals(MSCManager.PhenotypeManager.class)
    }

}
