/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package RuleTests
import org.safetycode.msc3.representation.Guideline
import org.safetycode.msc3.representation.Profile
import spock.lang.Specification
/**
 * Created by Sebastian on 17.07.2015.
 */
class PhenotypeTests extends Specification {

    def "demo profile with TPMT *2/*3A"() {
        when:
        def profile = new Profile( // Haplotypes            Guidelines
                "000101" + // ABCB1 *1 / *1
                        "100101" + // ADRB H1 / H1
                        "200101" + // BRCA1 1 / 1
                        "300101" + // COMT Haplotype high activity / Haplotype high activity
                        "400101" + // CYP1A2 *1A / *1A
                        "500101" + // CYP2A6 *1A / *1A
                        "600101" + // CYP2B6 *1 / *1
                        "700101" + // CYP2C19 *1 / *1
                        "800101" + // CYP2C9 *1 / *1
                        "900108" + // CYP2D6 *1 / * 2
                        "A00101" + // CYP3A4 *1 / *1
                        "B00101" + // CYP3A5 *1 / *1
                        "C00101" + // DPYD *1 / *1
                        "D00101" + // G6PD B (wildtype) / B (wildtype)
                        "G00101" + // HMGCR H2 / H7
                        "H00101" + // P2RY12 H1 / H1
                        "J00102" + // SULT1A1 *1 / *2
                        "K50405" + // TPMT *2 / *3A         36, 38, 40         <-- *2/*3A
                        "L00101" + // UGT1A1 *1a / *1a
                        "M00102" ) // VKORC *1 / *2
        def importantGuidelines = profile.getImportantGuidelines()

        then:
        importantGuidelines.size() == 3
        importantGuidelines.containsAll(Guideline.get(Arrays.asList(
                "cds_rule_36","cds_rule_38", "cds_rule_40", )))
    }

    def "demo profile: TPMT PM, CYP2D6 UM, CYP2C19 PM"() {
        when:
        def profile = new Profile( // Haplotypes            Guidelines
                "000101" + // ABCB1 *1 / *1
                        "100101" + // ADRB H1 / H1
                        "200101" + // BRCA1 1 / 1
                        "300101" + // COMT Haplotype high activity / Haplotype high activity
                        "400101" + // CYP1A2 *1A / *1A
                        "500101" + // CYP2A6 *1A / *1A
                        "600101" + // CYP2B6 *1 / *1
                        "75050A" + // CYP2C19 *2 / *3                           <-- DPWG PM
                        "800101" + // CYP2C9 *1 / *1
                        "970101" + // CYP2D6 *1 / * 1                           <-- DPWG UM, TODO: fix this with CNVs in haplotypes
                        "A00101" + // CYP3A4 *1 / *1
                        "B00101" + // CYP3A5 *1 / *1
                        "C00101" + // DPYD *1 / *1
                        "D00101" + // G6PD B (wildtype) / B (wildtype)
                        "G00101" + // HMGCR H2 / H7
                        "H00101" + // P2RY12 H1 / H1
                        "J00102" + // SULT1A1 *1 / *2
                        "K50405" + // TPMT *2 / *3A                             <-- DPWG PM
                        "L00101" + // UGT1A1 *1a / *1a
                        "M00102" ) // VKORC *1 / *2
        def importantGuidelines = profile.getImportantGuidelines();

        then:
        importantGuidelines.size() == 15
    }

    def "CPIC CYP2C9 ultrarapid metabolizer"() {
        expect:
        new Profile("810000").getGuidelines().size() == 0
    }

    def "FDA VKORC1 rs9923231 GG"() {
        expect:
        new Profile("M10000").getGuidelines().size() == 0
    }

    def "CPIC CYP2C9 *1/*1 and FDA VKORC1 rs9923231 GG"() {
        when:
        def profile = new Profile("810000M10000")
        def guidelines = profile.getGuidelines()
        def importantGuidelines = profile.getImportantGuidelines()

        then:
        guidelines.size() == 1
        guidelines.contains(Guideline.get("cds_rule_1"))
        importantGuidelines.size() == 1
        importantGuidelines.contains(Guideline.get("cds_rule_1"))
    }

}
